# Ferris Engine
> A browser engine written in Rust.
> This browser engine was developed following [Matt Brubecks](https://limpet.net/mbrubeck/) blog series 'Let's build a browser engine!'

![](header.png)

## Installation

1. Install Rust 1.0 beta or newer
1. Run ```cargo build``` to build and ```cargo run``` to run it.

To build and run with optimizations enabled, use ```cargo build --release``` and ```cargo run --release```.

## Usage example

By default, will load ```test.html``` and ```test.css``` from the examples directory. You can use the ```--html``` and ```--css``` arguments to the executable to change the input files:

```./target/debug/ferris-engine --html examples/test.html --css examples/test.css```

The rendered page will be saved to a file named ```output.png```. To change the output filename, use the ```-o``` option. To switch to PDF output, use add ```--format``` pdf.

