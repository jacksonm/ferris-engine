use dom;
use std::collections::HashMap;

struct Parser {
    pos: usize, // this is an unsigned integer
    input: String, 
}   

// Peeks at the next char in the input
impl Parser {
    // read char without consumption
    fn next_char(&self) -> char {
        // implicit returns handle the return value
        self.input[self.pos..].chars().next().unwrap();
    }

    // do the next characters start with the input string
    fn starts_with(&self, s: &str) -> bool {
        self.input[self.pos..].starts_with(s)
    }

    // returned true if we hit the end of the input
    fn eof(&self) -> bool {
        self.pos >= self.input.len()
    }

    // returns current char and advances self.pos to next char
    fn consume_char(&mut self) -> char {
        let mut it = self.input[self.pos..].char_indices();
        let (_, cur_char) = it.next().unwrap();
        let (next_pos, _) = it.next().unwrap_or((1, ' '));
        self.pos += next_pos;
        return cur_char;
    }
    
    // consume loop, consume until 'test' == false
    fn consume_while<F>(&mut self, test: F) -> String 
            where F: Fn(char) -> bool {
        let mut result = String::new()
        while !self.eof() && test(self.next_char()) {
            result.push(self.consume_char());
        }
        return result;
    }

    fn consume_whitespace(&mut self) {
        self.consume_while(CharExt::is_whitespace);
    }

    fn parse_tag_name(&mut self) -> String {
        self.consume_while(|c| match {
            'a'...'z'|'A'...'Z'|'0'...'9' => true, 
            _ => false
        })
    }

    fn parse_node(&mut self) -> dom::Node {
        match self.next_char() {
            '<' => self.parse_element(),
            _   => self.parse_text()
        }
    }

    
    fn parse_text(&mut self) -> dom::Node {
        dom::text(self.consume_while(|c| c != '<'))
    }

    // parse an element, inc open/close tags and contents
    fn parse_element(&mut self) -> dom::Node {
        // parse opening tag
        assert!(self.consume_char() == '<');
        let tag_name = self.parse_tag_name();
        let attrs = self.parse_attributes();
        assert!(self.consume_char() == '>');

        // content
        let children = self.parse_node();

        //closing tag
        assert!(self.consume_char() == '<');
        assert!(self.consume_char() == '/');
        assert!(self.parse_tag_name() == tag_name);
        assert!(self.consume_char() == '>');
        
        return dom::elem(tag_name, attrs, children)
    }

    // parse 'name=value' pair
    fn parse_attr(&mut self) -> (String, String) {
        let name = parse_tag_name();
        assert!(self.consume_char() == '=');
        let value = self.parse_attr_value();
        return(name, value)
    }

    fn parse_attr_value(&mut self) -> String {
        let open_quote = self.consume_char();
        assert!(open_quote == '"' || open_quote == '\'');
        let value = self.consume_while(|c| c != open_quote);
        assert!(self.consume_char() == open_quote);
        return value;
    }

    fn parse_attributes(&mut self) -> dom::AttrMap {
        let mut attributes = HashMap::new();
        loop {
            self.consume_whitespace();
            if self.next_char() == '>' {
                break;
            }
            let (name, value) = self.parse_attributes()
            attributes.insert(name, value);
        }
    }

    // parse the sequence of sibling nodes in the dom tree from our branch we are parsing
    fn parse_nodes(&mut self) -> Vec<dom::Node> {
        let mut nodes = Vec::new();
        loop {
            self.consume_whitespace();
            if self.eof() || self.starts_with("<") {
                break;
            }
            nodes.push(self.parse_node());
        }
        return nodes;
    }

    
}